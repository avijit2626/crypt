
import Foundation
import CRDCrypt

internal class EncryptData {


    //Shared instance.
    static internal let shared = EncryptData()


///    Encrypt the given string using the AES256CBC encryption technique.
    internal func encryptString(withString string: String, iv: Data, saltKey: String) -> (isSuccess: Bool, data: String?) {
        var encryptedDataWithoutBase64: Data? = nil

        do {
            encryptedDataWithoutBase64 = try string.data(using: .utf8)?.aes256Encrypt(withKey: saltKey, initializationVector: iv)
        }catch {
            return (false, nil)
        }
        let encryptedStringBase64 = encryptedDataWithoutBase64!.base64EncodedString()
        return (true, encryptedStringBase64)
    }


///    Decrypt the given string using the AES256CBC decryption technique.
    internal func decryptData(withString string: String, iv: Data, saltKey: String) -> (isSuccess: Bool, data: String?) {

        var decryptedString: String? = nil
        let encryptedData = Data(base64Encoded: string)

            do {
                decryptedString = String(data: try encryptedData!.aes256Decrypt(withKey: saltKey, initializationVector: iv), encoding: .utf8)
            } catch {
                return (false, nil)
            }

        guard decryptedString != nil else {
            return (false, nil)
        }
        return (true, decryptedString!)
    }


}
