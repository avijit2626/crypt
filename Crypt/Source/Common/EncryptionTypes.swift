import Foundation

public enum EncryptionTypes: String, CaseIterable {
    case AES256CBC
}
