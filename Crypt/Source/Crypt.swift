
import Foundation

public class Crypt {

    ///Shared instance
    static public let shared = Crypt()



    /// Encrypt Text with the providing iv key, salt key and Encryption types.
    public func encrypText(text: String, iv: Data, saltkey: String ,withTypes type: EncryptionTypes) -> (isSuccess: Bool, data: String?) {

        switch type {
        case .AES256CBC:
            return EncryptData.shared.encryptString(withString: text, iv: iv, saltKey: saltkey)
            
        }
    }


    /// Decrypt Text with the providing iv key, salt key and Encryption types.
    public func decryptText(text: String, iv: Data, saltkey: String ,withTypes type: EncryptionTypes) -> (isSuccess: Bool, data: String?) {

        switch type {
        case .AES256CBC:
            return EncryptData.shared.decryptData(withString: text, iv: iv, saltKey: saltkey)
        }
    }
}
