Pod::Spec.new do |spec|
  spec.name         = "Crypt"
  spec.version      = "1.0.0"
  spec.summary      = "Encrypt & Decrypt Data Using Different Encryption Methods"
  spec.description  = <<-DESC
                    Crypt allows to encrypt and decrypt data using different encryption methods i.e, AES256CBC. This library can be customised to add your own encryption method.
                   DESC
  spec.homepage     = "https://daffodilsw.com"
  spec.license      = { :type => "MIT", :file => "LICENSE.txt" }
  spec.author       = { "Avijit Goswami" => "avijit.goswami@daffodilsw.com" }
  spec.platform     = :ios, "11.4"
  spec.source       = { :git => "https://bitbucket.org/avijit2626/crypt.git", :tag => "#{spec.version}" }
  spec.source_files = "Crypt/**/*.{swift}"
  spec.swift_version = "5.0"
  spec.dependency 'CRDCrypt'
end